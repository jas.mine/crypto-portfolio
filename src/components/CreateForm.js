import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { emailChanged, passwordChanged, createUser } from '../actions';
import { Input, Spinner } from './common';

class CreateForm extends Component {
  onEmailChange(text) {
    this.props.emailChanged(text);
  }

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }

  onButtonPress() {
    const { email, password } = this.props;

    this.props.createUser({ email, password });
  }

  renderError() {
    if (this.props.error) {
      return (
        <View style={{ backgroundColor: 'white' }}>
          <Text style={styles.errorTextStyle}>
            {this.props.error}
          </Text>
        </View>
      );
    }
    return null;
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <TouchableOpacity
        style={{ alignItems: 'center', justifyContent: 'center' }}
        onPress={() => this.onButtonPress()}
      >
        <View
          style={styles.loginButtonContainer}
        >
          <Text style={styles.loginButtonText}>CREATE ACCOUNT</Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.backgroundContainer}>
        <View style={{ padding: 10 }}>
          <Text style={styles.header}>SIGN UP</Text>
        </View>
        <View style={styles.inputsContainer}>
          <View style={styles.cardContainer}>
            <Input
              label="Email"
              placeholder="example@email.com"
              onChangeText={text => this.onEmailChange(text)}
              value={this.props.email}
            />
          </View>
          <View style={styles.cardContainer}>
            <Input
              secureTextEntry
              label="Password"
              placeholder="password"
              onChangeText={text => this.onPasswordChange(text)}
              value={this.props.password}
            />
          </View>
        </View>
        <View style={styles.buttonStyle}>
          <View style={styles.smallContainer}>
            {this.renderError()}
          </View>
        </View>
        <View style={styles.buttonStyle}>
          <View style={styles.smallContainer}>
            {this.renderButton()}
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red',
  },
  backgroundContainer: {
    flex: 1,
    backgroundColor: '#e7ebef',
  },
  inputsContainer: {
    paddingTop: 10,
    margin: 15,
    borderRadius: 4,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowRadius: 2,
    shadowOpacity: 0.4,
    shadowColor: '#C9CACC',
    backgroundColor: 'white',
  },
  buttonStyle: {
    alignItems: 'center',
  },
  cardContainer: {
    padding: 15,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
  },
  smallContainer: {
    padding: 10,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
  },
  header: {
    fontWeight: 'bold',
    fontSize: 26,
    color: '#070F51',
    textAlign: 'center',
  },
  loginButtonContainer: {
    justifyContent: 'center',
    backgroundColor: '#070F51',
    alignItems: 'center',
    width: 300,
    height: 45,
    padding: 15,
    borderRadius: 20,
    shadowOffset: { width: 3, height: 3 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
  },
  loginButtonText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: 'white',
  },
};

const mapStateToProps = ({ auth }) => {
  const {
    email, password, error, loading,
  } = auth;

  return {
    email,
    password,
    error,
    loading,
  };
};
export default connect(mapStateToProps, {
  emailChanged, passwordChanged, createUser,
})(CreateForm);
