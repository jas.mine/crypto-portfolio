import React from 'react';
import { Text, View } from 'react-native';

const GlobalDetail = ({ coinProp }) => {
  // Destructure references for nicer code
  const { total_market_cap_usd } = coinProp;
  const {
    headerContentLeftStyle,
  } = styles;

  numberWithSpaces = x => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');

  return (

    <View style={headerContentLeftStyle}>
      <Text style={styles.textStyle}>Global Market Cap</Text>
      <Text style={styles.textValueStyle}>{`$${numberWithSpaces(total_market_cap_usd)}`}</Text>
    </View>
  );
};

const styles = {
  headerContentLeftStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    padding: 20,
    margin: 10,
    borderRadius: 10,
    shadowOffset: {
      width: 3,
      height: 3,
    },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    backgroundColor: '#070F51',
  },
  textStyle: {
    justifyContent: 'center',
    color: 'white',
    fontSize: 18,
    marginBottom: 20,
  },
  textValueStyle: {
    justifyContent: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 24,
  },
};
export default GlobalDetail;
