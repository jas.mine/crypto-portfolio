import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Google, Facebook } from 'expo';
import { View, Text, TouchableOpacity, TouchableHighlight } from 'react-native';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import {
  emailChanged,
  passwordChanged,
  loginUser,
} from '../actions';
import { Input, Spinner } from './common';

class LoginForm extends Component {
  onEmailChange(text) {
    this.props.emailChanged(text);
  }

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }

  onButtonPress() {
    const { email, password } = this.props;

    this.props.loginUser({ email, password });
  }

  onForgotPress = () => {
    Actions.forgotPassword();
  };

  renderError() {
    if (this.props.error) {
      return (
        <View style={{ backgroundColor: 'white' }}>
          <Text style={styles.errorTextStyle}>{this.props.error}</Text>
        </View>
      );
    }
    return null;
  }

  renderButton() {
    if (this.props.loadingLogin) {
      return <Spinner size="large" />;
    }

    return (
      <TouchableOpacity
        style={{ alignItems: 'center', justifyContent: 'center' }}
        onPress={() => this.onButtonPress()}
      >
        <View style={styles.loginButtonContainer}>
          <Text style={styles.loginButtonText}>LOGIN</Text>
        </View>
      </TouchableOpacity>
    );
  }

  renderForgotButton() {
    return (
      <TouchableOpacity
        style={{ alignItems: 'center', justifyContent: 'center' }}
        onPress={() => this.onForgotPress()}
      >
        <View
          style={{ justifyContent: 'center', alignItems: 'center' }}
        >
          <Text style={{ fontSize: 18, color: '#070F51' }}>Forgot password</Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.backgroundContainer}>
        <View style={{ padding: 10 }}>
          <Text style={styles.header}>LOG IN</Text>
        </View>
        <View style={{ padding: 15 }}>
          <View style={{ margin: 5, backgroundColor: '#d63b3b' }}>
            <TouchableHighlight
              style={styles.fbBtn}
              onPress={() => {
                    Google.logInAsync({
                        iosClientId: '469477164254-p76i4s3m4suanspo6b6qkt907hmf711u.apps.googleusercontent.com',
                        scopes: ['profile', 'email'],
                    })
                    .then(res => console.log(res))
                    .catch(err => console.log(err));
                }}
            >
              <View style={{ flexDirection: 'row', paddingHorizontal: 15, paddingVertical: 5 }}>
                <Icon
                  name="google-"
                  type="entypo"
                  color="#ffffff"
                />
                <Text style={styles.gglogin}>
                  Login with Google
                </Text>
              </View>
            </TouchableHighlight>
          </View>
          <View style={{ margin: 5, backgroundColor: '#3B5998' }}>
            <TouchableHighlight
              style={styles.fbBtn}
              onPress={() => {
                Facebook.logInWithReadPermissionsAsync('808673949312875', {
                    behavior: 'web',
                })
                    .then(result =>
                          this.props.userActions.facebookToken(
                              result.token,
                              this.props.userActions,
                    )).catch(err => console.log(err));
                }}
            >
              <View style={{ flexDirection: 'row', paddingHorizontal: 15, paddingVertical: 5 }}>
                <Icon
                  name="facebook"
                  type="entypo"
                  color="#ffffff"
                />
                <Text style={styles.gglogin}>
                   Login with Facebook
                </Text>
              </View>
            </TouchableHighlight>
          </View>
        </View>
        <View style={styles.inputsContainer}>
          <View style={styles.cardContainer}>
            <Input
              label="Email"
              placeholder="example@email.com"
              placeholderTextColor="C9CACC"
              onChangeText={text => this.onEmailChange(text)}
              value={this.props.email}
            />
          </View>
          <View style={styles.smallContainer}>
            <Input
              secureTextEntry
              label="Password"
              placeholder="password"
              placeholderTextColor="C9CACC"
              onChangeText={text => this.onPasswordChange(text)}
              value={this.props.password}
            />
          </View>
        </View>
        <View style={styles.buttonStyle}>
          <View style={styles.smallContainer}>{this.renderError()}</View>
        </View>
        <View style={styles.buttonStyle}>
          <View style={styles.smallContainer}>{this.renderButton()}</View>
        </View>
        <View style={{ flex: 2 }}>
          <View style={styles.forgotButtonStyle}>
            <View style={styles.smallContainer}>
              {this.renderForgotButton()}
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red',
  },
  backgroundContainer: {
    flex: 1,
    backgroundColor: '#e7ebef',
  },
  inputsContainer: {
    paddingTop: 10,
    paddingBottom: 10,
    margin: 15,
    borderRadius: 4,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowRadius: 2,
    shadowOpacity: 0.4,
    shadowColor: '#C9CACC',
    backgroundColor: 'white',
  },
  buttonStyle: {
    alignItems: 'center',
  },
  cardContainer: {
    padding: 15,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
  },
  smallContainer: {
    padding: 10,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
  },
  loginButtonContainer: {
    justifyContent: 'center',
    backgroundColor: '#070F51',
    alignItems: 'center',
    width: 300,
    height: 45,
    padding: 15,
    borderRadius: 20,
    shadowOffset: { width: 3, height: 3 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
  },
  loginButtonText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: 'white',
  },
  forgotButtonStyle: {
    alignItems: 'center',
    height: 100,
  },
  header: {
    fontWeight: 'bold',
    fontSize: 26,
    color: '#070F51',
    textAlign: 'center',
  },
  gglogin: {
    paddingLeft: 50,
    fontSize: 18,
    color: 'white',
  },
};

const mapStateToProps = ({ auth }) => {
  const {
    email, password, error, loadingLogin,
  } = auth;

  return {
    email,
    password,
    error,
    loadingLogin,
  };
};
export default connect(
  mapStateToProps,
  {
    emailChanged,
    passwordChanged,
    loginUser,
  },
)(LoginForm);
